import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ImagePathPipe} from './pipes/image-path.pipe';



@NgModule({
  declarations: [ImagePathPipe],
  exports: [
    ImagePathPipe
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
