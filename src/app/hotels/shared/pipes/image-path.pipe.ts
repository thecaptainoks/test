import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ImagePathPipe'
})
export class ImagePathPipe implements PipeTransform {

  constructor() {
  }

  transform(path: string): string {
    if (!path) {
      return null;
    }
    return 'https://img1.night2stay.com' + path;
  }

}
