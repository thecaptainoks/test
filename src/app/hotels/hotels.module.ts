import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HotelsComponent} from './hotels.component';
import {FormsModule} from '@angular/forms';
import {HotelModule} from './hotel/hotel.module';
import {SharedModule} from './shared/shared.module';


@NgModule({
  declarations: [
    HotelsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HotelModule,
    SharedModule
  ],
  exports: [HotelsComponent]
})
export class HotelsModule {
}
