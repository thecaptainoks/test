import {Component, Input, OnInit} from '@angular/core';
import {HotelInfo} from '../models/hotel.model';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit {

  @Input()
  hotel: HotelInfo;
  constructor() { }

  ngOnInit() {
  }

  generateStars(stars: number): string {
    return '_' + (stars + 3) + '-stars';
  }

}
