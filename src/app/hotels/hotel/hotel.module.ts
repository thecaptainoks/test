import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HotelComponent} from './hotel.component';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  declarations: [HotelComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [HotelComponent]
})
export class HotelModule { }
