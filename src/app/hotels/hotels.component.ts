import {AfterViewInit, Component, OnInit} from '@angular/core';
import {WebsocketService} from './services/websocket.service';
import {Data} from './models/hotel.model';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {
  data = null;
  searchParams = {
    action: 'accommodation',
    data: {
      place: {
        in: 'CI005575LO'
      },
      date: {
        in: null,
        out: null
      },
      families: [{
        adults: 2
      }
      ],
      lastid: 0,
      num: 5
    },
    key: '58fd27fa-4f81-4e67-a6bf-0e0e3fe4d876',
    type: 'service'
  };

  dateIn = '';
  dateOut = '';

  constructor(public ws: WebsocketService) {
    this.ws = ws;
  }

  ngOnInit() {
  }

  getHotels() {
    this.searchParams.data.date.in = Date.parse(this.dateIn);
    this.searchParams.data.date.out = Date.parse(this.dateOut);
    this.ws.fetchHotels(JSON.stringify(this.searchParams));
  }


  getdata(): Data {
    const data = this.ws.data.getValue();
    if (data.hasOwnProperty('data')) {
      return data.data;
    } else {
      return null;
    }
  }
}
