export interface Data {
  readonly key: string;
  readonly status: number;
  readonly done: boolean;
  readonly found: number;
  readonly hash: string;
  readonly total: number;
  readonly exptime: number;
  readonly search: Array<HotelInfo>;
}

export interface HotelInfo {
  readonly info: {
    readonly point: Array<number>;
    readonly addr: string;
    readonly name: string;
    readonly img: string;
    readonly cat: number;
  };
  readonly items: Array<Array<Items>>;
}
export interface Items {
  readonly type: string;
  readonly meal: string;
  readonly commerce: {
    readonly currency: number;
    readonly discount: number;
    readonly offer: string;
    readonly original: number;
    readonly payment: number
    readonly providerid: string;
    readonly reservationfee: number;
    readonly toriginal: number;
    readonly tpayment: number;
  };
}
