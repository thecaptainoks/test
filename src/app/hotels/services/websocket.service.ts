import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  public readonly WS_URL = 'wss://api.night2stay.com/api/v2/websocket';
  private readonly KEY = '4bd97223-9ad0-4261-821d-3e9ffc356e32';
  private readonly STATUS_OK = 200;
  private ws = null;

  public data: BehaviorSubject<any>;

  constructor() {
    this.data = new BehaviorSubject<any>({});
    this.ws = new WebSocket(this.WS_URL);
    this.ws.onopen = () => {
      this.ws.onmessage = (event) => {
       // if (this.KEY === JSON.parse(event.data).key && JSON.parse(event.data).status === this.STATUS_OK) {
          this.data.next(JSON.parse(event.data));
       // }
      };
      this.ws.send(
        '{' +
        '"action":"login",' +
        '"data":{"key":"123123 ", "wlcompany": "CMPN223463HE"},' +
        '"key":"4bd97223-9ad0-4261-821d-3e9ffc356e32",' +
        '"type":"account"' +
        '}'
      );
    };
  }

  fetchHotels(hotels: string) {
    this.ws.send(hotels);
  }
}
